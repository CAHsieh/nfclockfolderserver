package org.cahsieh.Widget;

import java.io.File;

/**
 * Created by CANCHU on 2016/6/6.
 */
public class FileLoader {

    public static String getFirstLayerFileList() {
        return getAssignFolderList("");
    }

    public static String getAssignFolderList(String folderName) {
        File folder = new File("E:\\MySecretFolder\\" + folderName);
        String result = "";
        if (folder.exists()) {
            for (File fileEntry : folder.listFiles()) {
                if (fileEntry.isDirectory()) {
                    result = result + fileEntry.getName() + "?folder*";
                }
            }

            for (File fileEntry : folder.listFiles()) {
                if (!fileEntry.isDirectory()) {
                    result = result + fileEntry.getName() + "?file*";
                }
            }
        }
        return result;
    }
}
