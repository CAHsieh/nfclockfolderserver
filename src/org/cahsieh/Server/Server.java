package org.cahsieh.Server;

import org.cahsieh.Widget.FileLoader;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import static java.lang.System.out;

/**
 * Created by CANCHU on 2016/6/6.
 */
public class Server {

    private boolean isAlive;
    private final ServerSocket serverSocket;

    private Server() throws IOException {
        isAlive = true;
        int PORT = 5566;
        serverSocket = new ServerSocket(PORT);
    }

    private void start() {
        new Thread(() -> {
            Socket socket;
            BufferedInputStream inputStream;
            out.println("Server has been Started.");
            while (isAlive) {
                socket = null;
                try {
                    synchronized (serverSocket) {
                        socket = serverSocket.accept();
                    }
                    out.println("Get Connected : InetAddress = " + socket.getInetAddress());
                    socket.setSoTimeout(15000);
                    inputStream = new BufferedInputStream(socket.getInputStream());
                    byte[] buffer = new byte[1024];
                    int len;
                    String command = "";
                    while ((len = inputStream.read(buffer)) > 0) {
                        command = command + new String(buffer, 0, len,"UTF-8");
                    }
                    socket.shutdownInput();
                    out.println("Get Command: " + command);
                    //TODO Command

                    String[] commands = command.split("&");
                    BufferedOutputStream outputStream = new BufferedOutputStream(socket.getOutputStream());
                    switch (commands[0]) {
                        case "GET_LIST":
                            if (commands.length == 1) {
                                String result = FileLoader.getFirstLayerFileList();
                                out.println("result: "+result);
                                outputStream.write(result.getBytes("UTF-8"));
                                outputStream.flush();
                                socket.shutdownOutput();
                            } else {
                                String result = FileLoader.getAssignFolderList(commands[1]);
                                out.println("result: "+result);
                                outputStream.write(result.getBytes("UTF-8"));
                                outputStream.flush();
                                socket.shutdownOutput();
                            }
                            break;
                        case "GET_FILE":
                            File file = new File("E:\\MySecretFolder\\" + commands[1]);
                            if (file.exists()) {
                                BufferedInputStream fileInput = new BufferedInputStream(new FileInputStream(file));
                                while ((len = fileInput.read(buffer)) > 0) {
                                    outputStream.write(buffer, 0, len);
                                }
                                outputStream.flush();
                                socket.shutdownOutput();
                                fileInput.close();
                            } else {
                                String result = "Assign file not found.";
                                out.println("result: "+result);
                                outputStream.write(result.getBytes("UTF-8"));
                                outputStream.flush();
                                socket.shutdownOutput();
                            }
                            break;
                    }

                    outputStream.close();
                    outputStream = null;
                    inputStream.close();
                    inputStream = null;
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void main(String[] args) {
        try {
            Server server = new Server();
            server.start();
            Scanner scanner = new Scanner(System.in);
            String input;
            do {
                input = scanner.nextLine();
            } while (!input.equals("exit"));
            server.isAlive = false;
        } catch (IOException e) {
            out.println("Server Socket Start Error.");
            e.printStackTrace();
        }
    }
}
